<?php
 
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
 
Route::get('/', [AuthController::class, 'showFormLogin'])->name('login');
Route::get('login', [AuthController::class, 'showFormLogin'])->name('login');
Route::post('login', [AuthController::class, 'login']);
Route::get('register', [AuthController::class, 'showFormRegister'])->name('register');
Route::post('register', [AuthController::class, 'register']);
 
Route::group(['middleware' => 'auth'], function () {
 
    Route::get('home', [HomeController::class, 'index'])->name('home');
    Route::get('home_pegawai', [HomeController::class, 'home_pegawai'])->name('home_pegawai');
    Route::get('absensi', [HomeController::class, 'absensi'])->name('absensi');
    Route::get('manajemen_user', [HomeController::class, 'manajemen_user'])->name('manajemen_user');
    Route::get('ubah_data', [HomeController::class, 'ubah_data'])->name('ubah_data');
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');
    Route::post('absensi_submit', [AuthController::class, 'absensi_submit'])->name('absensi_submit');
 
});