<?php
  
namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Session;
  
class HomeController extends Controller
{
    public function index()
    {
        return view('home');
    }

    public function home_pegawai()
    {
        return view('home_pegawai');
    }

    public function absensi()
    {
        $id_user = Session::get('id_user');
        // DB::enableQueryLog();//Enable query logging
        $absens = DB::table('absens')
        ->where('id_user', '=', $id_user)
        ->whereRaw('DATE(created_at) = CURDATE()')
        ->first();
        // dd(DB::getQueryLog());
        return view('absensi', ['absens' => $absens]);
    }

    public function manajemen_user()
    {
        return view('manajemen_user');
    }

    public function ubah_data()
    {
        return view('ubah_data');
    }
}