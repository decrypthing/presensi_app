<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Absen extends Model
{
    use HasFactory;

    protected $fillable = ["id_user", "long", "lat", "file_name", "file_path", "type",  "created_at", "updated_at"];
}
