
@extends('layout')

@section('title', 'Ubah Data')

@section('content')    

<div class="body">
    <style type="text/css">
      .body {
          width: 100%;
          min-height: 90vh;
        background-color: #fff;
        background: radial-gradient(circle at center, #fff 0%, #f8f8f8 75%, #ebebeb 100%);
        color: #222;
        font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, "Helvetica Neue", Arial, sans-serif;
        font-size: 1rem;
        line-height: 1.5;
        margin: 0;
        display: flex;
        justify-content: center;
        align-items: center;
      }
      .body main {
        padding: 1rem;
        text-align: center;
      }
      .body h1 {
        font-size: 2.5rem;
        line-height: 1.1;
        margin: 0;
      }
      @media screen and (max-width: 480px) {
       .body h1 {
          font-size: 1.5rem;
        }
      }
      .body h1::after {
        content: "";
        background-color: #ffe800;
        background: repeating-linear-gradient(45deg, #ffe800, #ffe800 0.5rem, #222 0.5rem, #222 1.0rem);
        display: block;
        height: 0.5rem;
        margin-top: 1rem;
      }
      .body p {
        margin: 1rem 0 0 0;
      }
    </style>
    <main>
      <h1>Sorry for this caution</h1>
      <h3>Our website is under construction.</h3>
    </main>

    </div>
@endsection