<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                    <img alt="image" class="img-circle" src="img/profile_small.jpg" />
                     </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ Auth::user()->name }}</strong>
                     </span> <span class="text-muted text-xs block">Pegawai </a>
                </div>
                <div class="logo-element">
                    APP
                </div>
            </li>
            <li class="{{ (request()->is('home*')) ? 'active' : '' }}">
                <a href="{{ route('home_pegawai') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span></a>
            </li>
            @if (Auth::user()->id_role == 2)
            <li class="{{ (request()->segment(1) == 'absensi') ? 'active' : '' }}">
                <a href="{{ route('absensi') }}"><i class="fa fa-pencil"></i> <span class="nav-label">Absensi</span></a>
            </li>
            {{-- <li class="{{ (request()->segment(1) == 'ubah_data') ? 'active' : '' }}">
                <a href="{{ route('ubah_data') }}"><i class="fa fa-refresh"></i> <span class="nav-label">Ubah Data</span></a>
            </li> --}}
            @endif
            @if (Auth::user()->id_role == 1)
            <li class="{{ (request()->segment(1) == 'manajemen_user') ? 'active' : '' }}">
                <a href="{{ route('manajemen_user') }}"><i class="fa fa-pencil"></i> <span class="nav-label">Manajemen User</span></a>
            </li>
            @endif
            <li>
                <a href="{{ route('logout') }}"><i class="fa fa-sign-out"></i> <span class="nav-label">Log out</span></a>
            </li>
        </ul>

    </div>
</nav>