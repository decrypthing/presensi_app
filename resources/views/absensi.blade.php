
@extends('layout')

@section('title', 'Main page')

@section('custom_css')
    <link href="css/plugins/steps/jquery.steps.css" rel="stylesheet">
    <link href="css/plugins/iCheck/custom.css" rel="stylesheet">    
@endsection

@section('content')

<style>
    .wizard > .content{
        min-height: 500px;overflow: auto;
    }
    .custom-map-control-button {
        background-color: #fff;
        border: 0;
        border-radius: 2px;
        box-shadow: 0 1px 4px -1px rgba(0, 0, 0, 0.3);
        margin: 10px;
        padding: 0 0.5em;
        font: 400 18px Roboto, Arial, sans-serif;
        overflow: hidden;
        height: 40px;
        cursor: pointer;
      }
      .custom-map-control-button:hover {
        background: #ebebeb;
      }
</style>
    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-lg-10 col-lg-offset-1">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        @if( is_null($absens))
                            <h5>Absen Check In</h5>
                        @elseif($absens)
                            <h5>Absen Check Out</h5>
                        @endif
                        
                        <div class="ibox-tools">
                            <a class="collapse-link">
                                <i class="fa fa-chevron-up"></i>
                            </a>
                        </div>
                    </div>
                    <div class="ibox-content">
                        <p>
                            Mohon isi data - data dibawah ini untuk melakukan 
                        @if( is_null($absens))
                            Check In
                        @elseif($absens)
                            Check Out
                        @endif
                        </p>
                        <form id="wizard" action="{{ route("absensi_submit") }}" method="post" enctype="multipart/form-data">
                            @csrf
                            <h1>Isi Foto Absen</h1>
                            <div class="step-content" style="padding: 0; width:100%; height:100%">
                                <!-- CSS -->
                                <style>
                                    #my_camera{
                                        width: 320px;   
                                        height: 240px;
                                        border: 1px solid black;
                                        float: left;
                                        margin: 5px;
                                    }
                                    </style>
                                    
                                    <div id="my_camera"></div>
                                     
                                    <div id="results" ></div>

                                    <br>
                                    <input type=button class="btn btn-md btn-info" value="Take Snapshot" onClick="take_snapshot()">
                                    
                                    <input type="text" class="hidden" name="file_path" id="file_path" value="">
                            </div>
                                
                            <h1>Isi Lokasi</h1>
                            <div class="step-content" style="padding: 0; width:100%; height:100%">
                                <div class="text-center" style="overflow: auto">
                                    <div id="map" style="width: 100%; height: 500px;"></div>
                                </div>
                                <input type="text" class="hidden" name="lat" id="lat" value="">
                                <input type="text" class="hidden" name="long" id="long" value="">
                            </div>

                            <h1>Absen</h1>
                            <div class="step-content">
                                <div class="text-center m-t-md">
                                    <div class="jumbotron">
                                        <h1>ABSENSI</h1>
                                        <p>Date : <span id="clockbox"></span></p>
                                        @if( is_null($absens))
                                            <input type="text" class="hidden" name="type" id="type" value="1">
                                        @elseif($absens)
                                            <input type="text" class="hidden" name="type" id="type" value="2">
                                        @endif
                                        <p><button type="submit" class="btn btn-primary btn-lg" role="button">
                                            @if( is_null($absens))
                                                Check In
                                            @elseif($absens)
                                                Check Out
                                            @endif
                                            <i class="fa fa-paper-plane"></i></button></p>
                                    </div>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
            {{-- <div class="col-lg-5">
               
            </div> --}}
        </div>
    </div>
@endsection

@section('custom_js')
     <!-- Steps -->
     <script src="js/plugins/steps/jquery.steps.min.js"></script>
     <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>

    <script src="js/plugins/validate/jquery.validate.min.js"></script>

    <script type="text/javascript" src="js/webcam.js"></script>
<script language="JavaScript">
    $(document).ready(function(){
        Webcam.set({
        width: 320,
        height: 240,
        image_format: 'jpeg',
        jpeg_quality: 90
        });
        Webcam.attach( '#my_camera' );
    });

    function take_snapshot() {
        
        // take snapshot and get image data
        Webcam.snap( function(data_uri) {
        // display results in page
        document.getElementById('results').innerHTML = 
        '<img src="'+data_uri+'"/>';

        document.getElementById('file_path').setAttribute("value", data_uri);
        } );
    }

    function initMap() {
  map = new google.maps.Map(document.getElementById("map"), {
    center: { lat: -6.175442, lng: 106.826702 },
    zoom: 17,
  });
  infoWindow = new google.maps.InfoWindow();
  const locationButton = document.createElement("button");
  locationButton.textContent = "Klik untuk lokasi saat ini";
  locationButton.setAttribute("type", "button");
  locationButton.classList.add("custom-map-control-button");
  map.controls[google.maps.ControlPosition.TOP_CENTER].push(locationButton);
  locationButton.addEventListener("click", () => {
    // Try HTML5 geolocation.
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          const pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude,
          };
          infoWindow.setPosition(pos);
          infoWindow.setContent("<strong style='font-size:17px;'><b><i class='fa fa-dot-circle-o' aria-hidden='true' style='color:blue'></i>&nbsp; Lokasi Kamu Saat Ini</b></strong>");
          infoWindow.open(map);
          map.setCenter(pos);
          document.getElementById("lat").setAttribute("value", position.coords.latitude)
          document.getElementById("long").setAttribute("value", position.coords.longitude)
        },
        () => {
          handleLocationError(true, infoWindow, map.getCenter());
        }
      );
    } else {
      // Browser doesn't support Geolocation
      handleLocationError(false, infoWindow, map.getCenter());
    }
  });
}

function handleLocationError(browserHasGeolocation, infoWindow, pos) {
  infoWindow.setPosition(pos);
  infoWindow.setContent(
    browserHasGeolocation
      ? "Error: The Geolocation service failed."
      : "Error: Your browser doesn't support geolocation."
  );
  infoWindow.open(map);
}
</script> 

<script
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDxDIVmiPsOKoZnTE4ko3cTYKTyHbyxvPo&callback=initMap&libraries=&v=weekly"
async></script>

<script>

    // $(document).ready(function(){

        // $("#wizard").steps();
        
//    });

var settings = {
    labels: {
        current: "current step:",
        pagination: "Pagination",
        finish: "Finish",
        next: "Selanjutnya",
        previous: "Kembali",
        loading: "Loading ..."
    },
    enableFinishButton: false
};
$("#wizard").steps(settings);

</script>

<script type="text/javascript">
    tday=new Array("Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu");
    tmonth=new Array("Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September","Oktober","November","Desember");
    
    function GetClock(){
    var d=new Date();
    var nday=d.getDay(),nmonth=d.getMonth(),ndate=d.getDate(),nyear=d.getYear(),nhour=d.getHours(),nmin=d.getMinutes(),nsec=d.getSeconds(),ap;
    
         if(nhour==0){ap=" AM";nhour=12;}
    else if(nhour<12){ap=" AM";}
    else if(nhour==12){ap=" PM";}
    else if(nhour>12){ap=" PM";nhour-=12;}
    
    if(nyear<1000) nyear+=1900;
    if(nmin<=9) nmin="0"+nmin;
    if(nsec<=9) nsec="0"+nsec;
    
    document.getElementById('clockbox').innerHTML=""+tday[nday]+", "+tmonth[nmonth]+" "+ndate+", "+nyear+" "+nhour+":"+nmin+":"+nsec+ap+"";
    }
    
    window.onload=function(){
    GetClock();
    setInterval(GetClock,1000);
    }
</script>
@endsection