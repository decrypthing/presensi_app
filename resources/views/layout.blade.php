<!--
*
*  INSPINIA - Responsive Admin Theme
*  version 2.7
*
-->

<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>PRESENSI APP - @yield('title')</title>

    <link rel="icon" href="Icon.png" type="image/png" sizes="16x16">

    @include('global.css')

</head>

<body>

  <!-- Wrapper-->
    <div id="wrapper">

        <!-- Navigation -->
        @include('global.navigation')

        <!-- Page wraper -->
        <div id="page-wrapper" class="gray-bg dashbard-1">

            <!-- Page wrapper -->
            @include('global.topnavbar')

            <!-- Main view  -->
            @yield('content')

            <!-- Footer -->
            @include('global.footer')

        </div>
        <!-- End page wrapper-->

    </div>
    <!-- End wrapper-->


@include('global.script')

</body>
</html>
